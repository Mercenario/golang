package main

import (
	"fmt"
	"gorilla/websocket"
	"log"
	"net/http"
)

var connectionUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	http.HandleFunc("/", handler)
	_ = http.ListenAndServe(":9090", nil)

}

func handler(w http.ResponseWriter, r *http.Request) {
	websocketConnection, err := connectionUpgrader.Upgrade(w, r, nil)
	defer websocketConnection.Close()

	if err != nil {
		log.Println("error=", err)
		return
	}

	for {
		_, p, _ := websocketConnection.ReadMessage()
		fmt.Println(string(p))
	}
}
