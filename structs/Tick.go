package structs

import "time"

type Tick struct {
	time       time.Time // Время последнего обновления цен
	bid        float64   // Текущая цена Bid
	ask        float64   // Текущая цена Ask
	last       float64   // Текущая цена последней сделки (Last)
	volume     uint64    // Объем для текущей цены Last
	timeMsc    int64     // Время последнего обновления цен в миллисекундах
	flags      uint32    // Флаги тиков
	volumeReal float64   // Объем для текущей цены Last c повышенной точностью
}

func NewTick(time time.Time, bid float64, ask float64, last float64, volume uint64, timeMsc int64, flags uint32, volumeReal float64) *Tick {
	return &Tick{time: time, bid: bid, ask: ask, last: last, volume: volume, timeMsc: timeMsc, flags: flags, volumeReal: volumeReal}
}
